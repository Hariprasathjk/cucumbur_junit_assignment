Feature: Validate the Login functionality of DemoWebshop Web Application

  Scenario Outline: Validate the Login functionality with Valid username and password
    Given User has to launch the web application demowebshop.tricentis.com through browser
    When User click the login button
    And User has to Enter the valid "<username>" and valild "<password>"
    And User has to click the Login button
    Then User should navigate to shopping page

    Examples: 
      | username                | password     |
      | hariprasathjk09@gmail.com   | ph@14Nov2022   |
      | xyk@gmail.com           | xyl@124      |